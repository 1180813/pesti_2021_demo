import { ExamDTO } from "../DTOs/ExamDTO";
import { ExamMapper } from "../mappers/ExamMapper";
import { ExamRepository } from "../repositories/ExamRepository";
import { Request } from "express";

export class ExamService {

    private repo: ExamRepository;

    constructor() {
        this.repo = new ExamRepository();
    }

    public async create(examDTO: ExamDTO) {
        var exam = ExamMapper.DTOtoDomain(examDTO);
        var createdExam = await this.repo.create(exam);
        var createdExamDTO = ExamMapper.domainToDTO(createdExam);

        return createdExamDTO;
    }

    public async getAllExams() {
        var examList = await this.repo.getAllExams();

        if (examList) {
            var examDTO = ExamMapper.domainListToDTO(examList);
            return examDTO;
        } else {
            return examList;
        }
    }

    public async findByKey(req: Request) {
        var examList = await this.repo.findByKey(req);

        if (examList) {
            var examDTO = ExamMapper.persistenceToDomain(examList);
            return examDTO;
        } else {
            return examList;
        }
    }

    public async findByModule(req: Request) {
        var examList = await this.repo.findByModule(req);
        
        if (examList) {
            //console.log(examList);
            var examDTO = ExamMapper.persistenceListToDomain(examList);
            return examDTO;
        } else {
            return examList;
        }
    }
}