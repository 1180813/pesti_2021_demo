export class Module {
    public module: String;

    constructor(module: String) {
        this.module = module;
    }

    get getModule(): String {
        return this.module;
    }
}